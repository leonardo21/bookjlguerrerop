﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace BookTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtTitulo.Text) && !lstNames.Items.Contains(txtTitulo.Text))
                lstNames.Items.Add(cmbCatergorias.Items[cmbCatergorias.SelectedIndex].ToString());
            XmlDocument doc = new XmlDocument();
            XmlElement raiz = doc.CreateElement("Libros");
            doc.AppendChild(raiz);

            XmlElement libro = doc.CreateElement("Libro");
            raiz.AppendChild(libro);

            XmlElement Titulo = doc.CreateElement("Título");
            Titulo.AppendChild(doc.CreateTextNode(txtTitulo.Text));
            XmlElement Autor = doc.CreateElement("Autor");
            Autor.AppendChild(doc.CreateTextNode(txtAutor.Text));
            XmlElement Editorial = doc.CreateElement("Editorial");
            Editorial.AppendChild(doc.CreateTextNode(txtEditorial.Text));
            XmlElement Categoria = doc.CreateElement("Categoría");
            Categoria.AppendChild(doc.CreateTextNode(cmbCatergorias.SelectedIndex.ToString()));

            doc.Save("c:\\Libros.xml");

        }
    }
}
